Survey.Survey.cssType = "bootstrap";

var json = {
    title: "Knowledge Test",
    showProgressBar: "bottom",
    showTimerPanel: "top",
    maxTimeToFinishPage: 10,
    maxTimeToFinish: 120,
    firstPageIsStarted: true,
    startSurveyText: "Start Quiz",
    pages: [
        {
            questions: [
                {
                    type: "html",
                    html: "You are about to start a true/flase quiz. <br/>You have 10 seconds for every question and 120 seconds for the whole survey of 10 questions.<br/>Please click on <b>'Start Quiz'</b> button when you are ready."
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Giving eye contact to the speaker is an example of ineffective listening.",
                    title: "Giving eye contact to the speaker is an example of ineffective listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Looking around the room while speaker is talking is effective listening.",
                    title: "Looking around the room while speaker is talking is effective listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Letting speaker finish and then talking is an example of ineffective listener.",
                    title: "Letting speaker finish and then talking is an example of ineffective listener.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Taking notes the whole time while the speaker is talking is effective listening.",
                    title: "Taking notes the whole time while the speaker is talking is effective listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Giving great eye contact while listening and then interrupting the speaker mid sentence to give a quick opinion is an effective listener.",
                    title: "Giving great eye contact while listening and then interrupting the speaker mid sentence to give a quick opinion is an effective listener.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Looking at your phone and responding to the speaker is an example of ineffective listening.",
                    title: "Looking at your phone and responding to the speaker is an example of ineffective listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Listen to the speaker's message, store it in your memory and start applying it is an effective listener.",
                    title: "Listen to the speaker's message, store it in your memory and start applying it is an effective listener.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Getting caught up on a word a speaker has said while they’re talking is an example of effective listening.",
                    title: "Getting caught up on a word a speaker has said while they’re talking is an example of effective listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Moving your legs while sitting when the speaker is talking is an effective listener.",
                    title: "Moving your legs while sitting when the speaker is talking is an effective listener.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "eye",
                    title: "Fight distraction is an example of ineffective listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        },
    ],
    completedHtml: "<h4>You have answered correctly <b>{correctedAnswers}</b> questions from <b>{questionCount}</b>.</h4>"
};

var survey = new Survey.Model(json);

survey
  .onComplete
  .add(function(result) {
    document
      .querySelector('#surveyResult')
      .innerHTML = "result: " + JSON.stringify(result.data);
  });

$("#surveyElement").Survey({
  model: survey
});
